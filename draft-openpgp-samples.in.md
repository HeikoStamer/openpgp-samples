---
title: OpenPGP Example Keys and Certificates
docname: draft-bre-openpgp-samples-02
date: 2019-12-20
category: info

ipr: trust200902
area: int
workgroup: openpgp
keyword: Internet-Draft

stand_alone: yes
pi: [toc, sortrefs, symrefs]

author:
 -
    ins: B. R. Einarsson
    name: Bjarni Rúnar Einarsson
    org: Mailpile ehf
    street: Baronsstig
    country: Iceland
    email: bre@mailpile.is
 -
    name: juga
    email: juga@riseup.net
    org: Independent
 -
    ins: D. K. Gillmor
    name: Daniel Kahn Gillmor
    org: American Civil Liberties Union
    street: 125 Broad St.
    city: New York, NY
    code: 10004
    country: USA
    abbrev: ACLU
    email: dkg@fifthhorseman.net
informative:
 OpenPGP-Email-Summit-2019:
    target:  https://wiki.gnupg.org/OpenPGPEmailSummit201910
    title: OpenPGP Email Summit 2019
    date: Oct 2019
 I-D.draft-dkg-openpgp-abuse-resistant-keystore-04:
normative:
 RFC2119:
 RFC8174:
 RFC4880:
 I-D.ietf-openpgp-rfc4880bis:
--- abstract

The OpenPGP development community benefits from sharing samples of signed or encrypted data. This document facilitates such collaboration by defining a small set of OpenPGP certificates and keys for use when generating such samples.

--- middle

Introduction
============

The OpenPGP development community, in particular the e-mail development community, benefits from sharing samples of signed and/or encrypted data. Often the exact key material used does not matter because the properties being tested pertain to implementation correctness, completeness or interoperability of the overall system. However, without access to the relevant secret key material, a sample is useless.

This document defines a small set of OpenPGP certificates and secret keys for use when generating or operating on such samples.

Samples are provided for three "personas", Alice, Bob, and Carol. Alice uses keys based on the Ed25519 elliptic curve algorithm, but Bob fears quantum computers that can attack the 256-bit ECC security and has a 3072-bit RSA key. Last but not least, Carol is a bit behind the times and has a DSA/ElGamal key.

Requirements Language
---------------------

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in BCP 14 {{RFC2119}} {{RFC8174}} when, and only when, they appear in all capitals, as shown here.

Terminology
-----------

This document makes use of the terminology section from {{I-D.draft-dkg-openpgp-abuse-resistant-keystore-04}}.


Alice's Ed25519 Samples
=======================

Properties:

   * OpenPGP Version: 4
   * Fingerprint: EB85 BB5F A33A 75E1 5E94 4E63 F231 550C 4F47 E38E
   * Primary key algorithm: Ed25519 {{I-D.ietf-openpgp-rfc4880bis}}
   * Primary key creation date: Tue Jan 22 11:56:25 GMT 2019
   * Primary key capabilities: certify, sign
   * User ID: `Alice Lovelace <alice@openpgp.example>`
   * Symmetric algorithm preferences: AES-256, AES-192, AES-128, 3DES
   * Hash algorithm preferences: SHA512, SHA384, SHA256, SHA224, SHA1
   * Compresson algorithm preferences: ZLIB, BZip2, ZIP
   * Subkey algorithm: Curve25519
   * Subkey capabilities: encrypt
   * Subkey creation date: Tue Jan 22 11:56:25 GMT 2019
   * There are no expiration dates in the entire certificate
   * The secret key material is in the clear (no password)
   * All OpenPGP signature packets contain a hashed Issuer Fingerprint
     subpacket (see {{I-D.ietf-openpgp-rfc4880bis}}


Alice's OpenPGP Certificate {#alice-cert}
---------------------------

~~~
@@alice@openpgp.example.pub.asc@@
~~~


Alice's OpenPGP Secret Key Material {#alice-key}
-----------------------------------

~~~
@@alice@openpgp.example.sec.asc@@
~~~


Alice's Revocation Certificate {#alice-rev}
------------------------------

~~~
@@alice@openpgp.example.rev.asc@@
~~~


Bob's RSA-3072 Samples
======================

Properties:

   * OpenPGP Version: 4
   * Fingerprint: D1A6 6E1A 23B1 82C9 980F 788C FBFC C82A 015E 7330
   * Primary key algorithm: RSA 3072 {{RFC4880}}
   * Primary key creation date: Tue Oct 15 10:18:26 GMT 2019
   * Primary key capabilities: certify, sign
   * User ID: `Bob Babbage <bob@openpgp.example>`
   * Symmetric algorithm preferences: AES-256, AES-192, AES-128, 3DES
   * Hash algorithm preferences: SHA512, SHA384, SHA256, SHA224, SHA1
   * Compresson algorithm preferences: ZLIB, BZip2, ZIP
   * Subkey algorithm: RSA 3072
   * Subkey capabilities: encrypt
   * Subkey creation date: Tue Oct 15 10:18:26 GMT 2019
   * There are no expiration dates in the entire certificate
   * The secret key material is in the clear (no password)
   * All OpenPGP signature packets contain a hashed Issuer Fingerprint
     subpacket (see {{I-D.ietf-openpgp-rfc4880bis}}


Bob's OpenPGP Certificate {#bob-cert}
-------------------------

~~~
@@bob@openpgp.example.pub.asc@@
~~~


Bob's OpenPGP Secret Key Material {#bob-key}
---------------------------------

~~~
@@bob@openpgp.example.sec.asc@@
~~~


Bob's Revocation Certificate {#bob-rev}
----------------------------

~~~
@@bob@openpgp.example.rev.asc@@
~~~

Carols's DSA/ElGamal Samples
============================

Properties:

   * OpenPGP Version: 4
   * Fingerprint: 71FF DA00 4409 E5DD B0C3 E8F1 9BA7 89DC 76D6 849A
   * Primary key algorithm: DSA 3072/256 {{RFC4880}}
   * Primary key creation date: Sat Dec 21 2019
   * Primary key capabilities: certify, sign
   * User ID: `Carol Oldstyle <carol@openpgp.example>`
   * Symmetric algorithm preferences: AES-256, AES-192, AES-128
   * Hash algorithm preferences: SHA512, SHA384, SHA256
   * Compresson algorithm preferences: ZIP
   * Subkey algorithm: ElGamal 3072/256
   * Subkey capabilities: encrypt
   * Subkey creation date: Sat Dec 21 2019
   * There are no expiration dates in the entire certificate
   * The secret key material is in the clear (no password)
   * All OpenPGP signature packets contain a hashed Issuer Fingerprint
     subpacket (see {{I-D.ietf-openpgp-rfc4880bis}}


Carol's OpenPGP Certificate {#carol-cert}
---------------------------

~~~
@@carol_openpgp.example.pub.asc@@
~~~


Carol's OpenPGP Secret Key Material {#carol-key}
---------------------------------

~~~
@@carol_openpgp.example.sec.asc@@
~~~

Security Considerations
=======================

The keys presented in this document should be considered compromised and insecure, because the secret key material is published and therefore not secret.

Applications which maintain blacklists of invalid key material SHOULD include these keys in their lists.


IANA Considerations
===================

IANA has nothing to do for this document.

Document Considerations
=======================

\[ RFC Editor: please remove this section before publication ]

This document is currently edited as markdown.  Minor editorial
changes can be suggested via merge requests at
https://gitlab.com/openpgp-wg/openpgp-samples or by e-mail to the
authors.  Please direct all significant commentary to the public IETF
OpenPGP mailing list: openpgp@ietf.org

Document History
----------------

Changes between -01 and -02:
 - added DSA/Elgamal example

Changes between -00 and -01:

 - converted to XML2RFC v3
 - added internal backreferences to sample material to spread awareness

Acknowledgements
================

The authors would like to acknowledge the help and input of the other
participants at the OpenPGP e-mail summit 2019
{{OpenPGP-Email-Summit-2019}}.
